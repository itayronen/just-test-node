# just-test-node
Just run and report.

Unfortunately, nodejs currently does NOT support <em>es6 imports</em> (it does support other es6 features).  
That means that your code(and tests) can target es6 (or later) but should be written/compiled/transpiled to CommonJs or UMD format for the module resolution to work on nodejs.  
Some more info about that will be after the usage.

## Install
npm install --save-dev just-test-node

## Usage
Write your test modules with the just-test-api (typescript interfaces):  
<https://www.npmjs.com/package/just-test-api>  

In your package.json:
```
"scripts": {
	"test": "gulp build && just-test",
	"testWithGlobExample": "just-test --glob=./lib/**/*.test.js"
}
```
The default glob will pick all "*.test.js" ("./!(node_modules)/**/*.test.js").

Run:  
`npm test`  
Or  
`npm test "some test filter"`

## Module - ES6, CommonJS, UMD, AMD, SystemJS, Webpack
I recommend you to read about the different module formats on the internet.  
The problem is that currently there is no one format to rule them all.  
* ES6 - Nodejs does not fully support es6 (import keyword).
  * Therefore node will run only CommonJs and UMD.
* CommonJS cannot be run on the browser out of the box. Some process or a supporting bundler should be used.
  * Webpack, for example, accepts commonjs, transform it, and bundles it to be run on the browser.
* UMD - Can be run on nodejs AND on the browser out of the box! But webpack will not accept it...
* AMD - Good for browsers (with require.js or system.js), but not for nodejs.
* Systemjs - Good for browsers (with system.js), but not for nodejs.
* Webpack - Will run on the browser. Can require/import: commonjs, amd, es6 formats. Requiring umd is not supported/bugged. There are plugins I haven't tried.

### Browser/Webpack users:
Notice that Webpack currently does NOT support UMD.  
If you are writing and testing a library that is intended to be bundled with Webpack,
I recommend you to create a CommonJs port of it.  
Another option is to output multiple formats. For example:
1. CommonJs format for node(and testing).
2. AMD format for browsers.

There are some tricks to create a js file that will support both AMD and CommonJS (and Webpack).  
These tricks require a wrapping function in every js file. This is ugly but works.  
This will probably destroy sourcemaps if not done with consideration (I don't know how to do it with consideration).

In summery, until node supports es6 "import" keyword, we suffer.
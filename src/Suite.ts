import { TestSuite, TestParams } from "just-test-api";
import { Event1 } from "itay-events";
import { Test, TestRunOptions } from "./Test";
import { TestRunResult } from "./run-results/TestRunResult";
import { SuiteResultStatus, SuiteRunResult } from "./run-results/SuiteRunResult";
import { DiscoveredSuite, FunctionCodeRange } from "./Discover";
import { getFunctionCodeRange } from "./code-position-resolution";

export interface RunParams {
	debug: boolean;
}

export interface TestEndedArgs {
	test: Test;
	result: TestRunResult;
}

export interface SuiteEndedArgs {
	suite: Suite;
	result: SuiteRunResult;
}

export class Suite implements TestSuite {
	public title: string = "";
	public location: FunctionCodeRange = null!
	public parent?: Suite;
	private isPartial = false;
	public tests: Test[] = [];

	private _childrenSuites: Suite[] = [];
	public childrenSuites: ReadonlyArray<Suite> = this._childrenSuites;

	public onSuiteStarted = new Event1<Suite>();
	public onSuiteEnded = new Event1<SuiteEndedArgs>();

	public onTestStarted = new Event1<Test>();
	public onTestEnded = new Event1<TestEndedArgs>();

	constructor() {
	}

	public discover(): DiscoveredSuite {
		return {
			title: this.title,
			location: this.location,
			tests: this.tests.map(e => e.discover()),
			suites: this.childrenSuites.map(e => e.discover())
		};
	}

	public describe(title: string | { name: string }, suiteFunction: (suite: Suite) => void): void {
		let childSuite = new Suite();
		childSuite.title = typeof (title) === "string" ? title : title.name;
		childSuite.location = getFunctionCodeRange(suiteFunction, this.describe);

		this.addChildSuite(childSuite);

		suiteFunction(childSuite);
	}

	public addChildSuite(childSuite: Suite): void {
		childSuite.onTestStarted.add(t => this.onTestStarted.trigger(t));
		childSuite.onTestEnded.add(result => this.onTestEnded.trigger(result));

		childSuite.onSuiteStarted.add(s => this.onSuiteStarted.trigger(s));
		childSuite.onSuiteEnded.add(s => this.onSuiteEnded.trigger(s));

		childSuite.parent = this;
		this._childrenSuites.push(childSuite);
	}

	public test(title: string, testFunction: (test: TestParams) => Promise<void> | void): void {
		const location: FunctionCodeRange = getFunctionCodeRange(testFunction, this.test);

		this.tests.push(new Test(title, testFunction, this, location));
	}

	public testFunc(namedTestFunction: (test: TestParams) => Promise<void> | void): void {
		const location: FunctionCodeRange = getFunctionCodeRange(namedTestFunction, this.testFunc);

		this.tests.push(new Test(this.getTitleFor(namedTestFunction), namedTestFunction, this, location));
	}

	private getTitleFor(func: Function): string {
		return func.name.replace(/_/g, " ");
	}

	public async run(params: RunParams): Promise<boolean> {
		this.onSuiteStarted.trigger(this);

		const startTime = performance.now();

		const testsPassed = await this.runDirectDescendantsTests(params);
		const subSuitesPassed = await this.runSubSuites(params);

		const passed = testsPassed && subSuitesPassed;

		this.onSuiteEnded.trigger({
			suite: this,
			result: new SuiteRunResult(this.title, performance.now() - startTime, this.getStatus(passed))
		});

		return passed;
	}

	private getStatus(runPassed: boolean): SuiteResultStatus {
		if (!runPassed)
			return SuiteResultStatus.Failed;

		if (this.isPartial)
			return SuiteResultStatus.Partial;

		return SuiteResultStatus.Passed;
	}

	public treeShake(lowerCasedFilter: string): boolean {
		if (this.isTitlePassingFilter(lowerCasedFilter))
			return true;

		const originalTestsCount = this.tests.length;
		const originalchildrenSuitesCount = this.childrenSuites.length;

		this.tests = this.tests.filter(e => e.title.toLowerCase().includes(lowerCasedFilter))
		this.childrenSuites = this.childrenSuites.filter(e => e.treeShake(lowerCasedFilter));

		this.isPartial = this.calcIsPartial(originalTestsCount, originalchildrenSuitesCount);

		return this.tests.length > 0 || this.childrenSuites.length > 0;
	}

	private calcIsPartial(originalTestsCount: number, originalchildrenSuitesCount: number): boolean {
		return this.tests.length < originalTestsCount ||
			this.childrenSuites.length < originalchildrenSuitesCount ||
			this.childrenSuites.some(e => e.isPartial)
	}

	private isTitlePassingFilter(lowerCasedFilter: string): boolean {
		return this.title.toLowerCase().includes(lowerCasedFilter.toLowerCase());
	}

	private async runDirectDescendantsTests(testRunOptions: TestRunOptions): Promise<boolean> {
		let passed = true;

		for (let test of this.tests) {
			this.onTestStarted.trigger(test);

			let result = await test.run(testRunOptions);
			passed = passed && result.passed;

			this.onTestEnded.trigger({ test, result });
		}

		return passed;
	}

	private async runSubSuites(params: RunParams): Promise<boolean> {
		let passed = true;

		for (let suite of this.childrenSuites) {
			let suitePassed = await suite.run(params);
			passed = passed && suitePassed;
		}

		return passed;
	}
}
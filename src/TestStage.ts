export enum TestStage {
    NoStage,
    Arrange,
    Act,
    Assert
}
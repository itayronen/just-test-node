import { OptionDefinition as ArgsOptionDefinition } from "command-line-args";

export interface Options {
	glob: string;
	filter: string;
	help: boolean;
	debug: boolean;
	discover: boolean;
	reporter: "spec" | "json";
	testFailureExitCode: number;
	directorySuitesGlob: string;
}

export function validateOptions(options: Options) {
	if (!["spec", "json"].includes(options.reporter))
		throw "invalid reporter.";
}

export interface OptionDefinition extends ArgsOptionDefinition {
	description: string;
}

export const optionsDefinitions: OptionDefinition[] = [
	{
		name: 'help', alias: 'h',
		type: Boolean,
		defaultValue: false,
		description: "Show help."
	},
	{
		name: 'glob',
		type: String,
		defaultValue: "./{*.test.js,!(node_modules)/**/*.test.js}",
		description: "Test files glob pattern."
	},
	{
		name: "filter",
		type: String,
		defaultOption: true,
		defaultValue: "",
		description: "Filters tests."
	},
	{
		name: 'debug',
		type: Boolean,
		defaultValue: false,
		description: "Debug mode. Will disable timeouts."
	},
	{
		name: 'discover',
		type: Boolean,
		defaultValue: false,
		description: "Outputs the available the tests as json."
	},
	{
		name: "reporter",
		type: String,
		defaultValue: "spec",
		description: `The reporter to use for ouput. Options: "spec", "json"`,
	},
	{
		name: "testFailureExitCode",
		type: Number,
		defaultValue: 1,
		description: `The exit code to use when has test failures.`,
	},
	{
		name: 'directorySuitesGlob',
		type: String,
		defaultValue: "./{@test-suite,!(node_modules)/**/@test-suite}",
		description: "Directory suite files glob pattern."
	}
];

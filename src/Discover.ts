export interface TextPosition {
	/**
	 * Zero based line.
	 */
	line: number;

	/**
	 * Zero based column.
	 */
	column: number;
}

export interface FunctionCodeRange {
	filePath: string;
	start: TextPosition;
	end: TextPosition;
}

export interface DiscoveredSuite {
	title: string;
	location: FunctionCodeRange;
	tests: DiscoveredTest[];
	suites: DiscoveredSuite[];
}

export interface DiscoveredTest {
	title: string;
	location: FunctionCodeRange;
}
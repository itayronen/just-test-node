import { TestParams } from "just-test-api";
import { DiscoveredTest, FunctionCodeRange } from "./Discover";
import { TestRunResult } from "./run-results/TestRunResult";
import { TestStage } from "./TestStage";
import { Suite } from "./Suite";
import { setStackRoot } from "./StackTrace";
import { stringifyReason } from "./reason-stringify";

class TestParamsClass implements TestParams {
	public stage = TestStage.NoStage;
	public timeout: number = 5000;

	constructor() {
	}

	public arrange(): void {
		this.stage = TestStage.Arrange;
	}

	public act(): void {
		this.stage = TestStage.Act;
	}

	public assert(): void {
		this.stage = TestStage.Assert;
	}
}

export interface TestRunOptions {
	debug: boolean;
}

export class Test {
	constructor(public title: string,
		public testFunction: (test: TestParams) => Promise<void> | void,
		public parent: Suite,
		public location: FunctionCodeRange
	) {
	}

	public discover(): DiscoveredTest {
		return { title: this.title, location: this.location };
	}

	public async run(options: TestRunOptions): Promise<TestRunResult> {
		const testParams = new TestParamsClass();
		const startTime = performance.now();
		const endLogsRecorder = this.startLogsRecorder();
		const restoreStackFunc = setStackRoot(callSite =>
			callSite.getTypeName() == Test.name &&
			callSite.getFunctionName() == this.run.name);

		try {
			let result = this.testFunction(testParams);

			if (this.isPromise(result)) {
				if (options.debug)
					await result;
				else
					await this.withTimeout(result!, testParams.timeout);
			}

			const logs = endLogsRecorder();

			return Promise.resolve(new TestRunResult(
				this.title, performance.now() - startTime, testParams.stage, logs, true));
		}
		catch (reason) {
			const logs = endLogsRecorder();
			const reasonText = stringifyReason(reason);

			return Promise.resolve(new TestRunResult(
				this.title, performance.now() - startTime, testParams.stage, logs, false, reasonText));
		}
		finally {
			restoreStackFunc();
		}
	}

	private startLogsRecorder(): () => string {
		let text = "";
		const originalLog = console.log;

		console.log = (...data: any[]) => {
			text += data.map(e => `${e}`).join(" ") + "\n";
		}

		return (): string => {
			console.log = originalLog;
			return text;
		}
	}

	private isPromise(result: any): boolean {
		return !!result && typeof result.then === "function";
	}

	private async withTimeout(promise: Promise<void>, timeout: number) {
		let timer: NodeJS.Timer = undefined!;

		let firstValue = await Promise.race([
			promise,
			new Promise<void | string>(resolve => {
				timer = setTimeout(() => resolve("timeout"), timeout);
			})
		]);

		if (typeof firstValue === "string" && firstValue === "timeout") {
			throw `Timeout (${timeout} milliseconds).`;
		}
		else {
			clearTimeout(timer);
		}
	}
}
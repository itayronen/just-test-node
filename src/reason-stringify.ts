export function stringifyReason(reason: unknown): string {
	if (typeof reason == "string")
		return reason;

	if (reason instanceof Error && reason.stack)
		return reason.stack;

	return String(reason);
}
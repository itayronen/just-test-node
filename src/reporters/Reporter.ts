import { Suite } from "../Suite";

export interface Reporter {
	listenTo(suite: Suite): void;
}
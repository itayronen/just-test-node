import chalk from "chalk";
import { Suite, TestEndedArgs } from "../Suite";
import { Test } from "../Test";
import { Reporter } from "./Reporter";

export class SpecReporter implements Reporter {
	private symbols = {
		ok: '✓',
		err: '✖',
	};

	private indentation = "  ";

	private passedCount = 0;
	private errors: TestEndedArgs[] = [];

	constructor(private output: Console) {
	}

	public listenTo(rootSuite: Suite): void {
		let indent = "";

		rootSuite.onSuiteStarted.add(suite => {
			if (suite == rootSuite)
				return;

			this.output.log();
			this.output.info(indent + suite.title);
			indent += this.indentation;
		});

		rootSuite.onSuiteEnded.add(args => {
			indent = indent.substr(0, indent.length - this.indentation.length);

			if (args.suite == rootSuite) {
				this.onEnd();
			}
		});

		rootSuite.onTestEnded.add(args => {
			const result = args.result;
			const elapsed = this.formatElapsed(result.elapsed);

			if (result.passed) {
				this.passedCount++;
				this.output.log(chalk.green(`${indent}${this.symbols.ok} ${result.title}`) + ` ${elapsed}`);
			}
			else {
				this.errors.push(args);
				this.output.log(chalk.red(`${indent}${this.symbols.err} ${result.title}`) + ` ${elapsed}`);
			}
		});
	}

	private onEnd(): void {
		if (!this.errors)
			return;

		this.output.log();
		this.output.log("Failed tests:");
		this.output.log("=============");

		for (let failedTestRun of this.errors) {
			this.output.log();

			let title = this.getTestFullTitle(failedTestRun.test);
			this.output.log(title);

			this.output.log(this.indentation + chalk.red(`${failedTestRun.result.reason}`));
		}

		this.output.log();
		this.output.log(chalk.green(`${this.passedCount} passing`));

		if (this.errors.length > 0) {
			this.output.log(chalk.red(`${this.errors.length} failing`));
		}

		this.output.log();
	}

	private formatElapsed(elapsed: number): string {
		return chalk.grey(`(${elapsed.toFixed(2)}ms)`);
	}

	private getTestFullTitle(test: Test): string {
		let titles = [test.title];

		let parent: Suite | undefined = test.parent;

		while (parent && parent.title) {
			titles.push(parent.title);
			parent = parent.parent;
		}

		let title = titles.pop()!;

		while (titles.length > 0) {
			title += " -> " + titles.pop();
		}

		return title;
	}
}
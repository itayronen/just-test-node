import { SuiteResultStatus, SuiteRunResult } from "../run-results/SuiteRunResult";
import { Test } from "../Test";
import { TestRunResult } from "../run-results/TestRunResult";
import { Suite } from "../Suite";
import { Reporter } from "./Reporter";

export interface SuiteResult {
	title: string;
	elapsed: number;
	status: SuiteResultStatus;
	tests: TestRunResult[];
	childrenSuites: SuiteResult[];
}

export class JsonReporter implements Reporter {
	private suiteToResultMap = new Map<Suite, SuiteRunResult>();
	private testToResultMap = new Map<Test, TestRunResult>();

	constructor(private output: Console) {
	}

	public listenTo(rootSuite: Suite): void {
		rootSuite.onSuiteEnded.add(args => {
			this.suiteToResultMap.set(args.suite, args.result);

			if (args.suite == rootSuite)
				this.outputResultsFor(rootSuite);
		});

		rootSuite.onTestEnded.add(args => {
			this.testToResultMap.set(args.test, args.result);
		});
	}

	private outputResultsFor(suite: Suite): void {
		const result = this.getResultOfSuite(suite);

		this.output.log(JSON.stringify(result));
	}

	private getResultOfSuite(suite: Suite): SuiteResult {
		const runResult = this.suiteToResultMap.get(suite)!;
		const tests = suite.tests.map(e => this.testToResultMap.get(e)!);
		const childrenSuites = suite.childrenSuites.map(e => this.getResultOfSuite(e)!);

		return {
			title: suite.title,
			status: runResult.status,
			elapsed: runResult.elapsed,
			tests: tests,
			childrenSuites: childrenSuites
		};
	}
}
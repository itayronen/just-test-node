import { FunctionCodeRange, TextPosition } from "./Discover";
import { getCallerLocationText } from "./StackTrace";

export function getFunctionCodeRange(func: Function, definingFunc: Function) {
	const { filePath, callPosition } = getCallerPosition(definingFunc);

	const endLocation = isInline(func) ?
		getInlineFuncEndPosition(func, callPosition) :
		callPosition;

	const location: FunctionCodeRange = { filePath, start: callPosition, end: endLocation };

	return location;
}

function getCallerPosition(func: Function): { filePath: string, callPosition: TextPosition } {
	const callSiteText = getCallerLocationText(func);

	const separator2 = callSiteText.lastIndexOf(":");
	const separator1 = callSiteText.lastIndexOf(":", separator2 - 1);

	const filePath = callSiteText.slice(0, separator1);
	const rowText = callSiteText.slice(separator1 + 1, separator2);
	const columnText = callSiteText.slice(separator2 + 1);

	const line = parseInt(rowText);
	const column = parseInt(columnText);

	return { filePath, callPosition: { line: line - 1, column: column - 1 } };
}

function getInlineFuncEndPosition(func: Function, start: TextPosition): TextPosition {
	const position = getFuncRelativeEndPosition(func);

	const endLine = start.line + position.line;

	const endColumn = position.line == 0 ?
		start.column :
		position.column;

	return { line: endLine, column: endColumn };
}

function getFuncRelativeEndPosition(func: Function): TextPosition {
	let line = 0;
	let column = 0;

	const text = func.toString();

	for (let i = 0; i < text.length; i++) {
		if (text[i] == "\n") {
			line++;
			column = 0;
		}

		column++;
	}

	return { line, column };
}

function isInline(func: Function): boolean {
	return func.name.length == 0;
}

export enum SuiteResultStatus{
	Passed = "passed",
	Failed = "failed",
	Partial = "partial",
}

export class SuiteRunResult {
	constructor(public title: string, public elapsed: number, public status: SuiteResultStatus) {
	}
}
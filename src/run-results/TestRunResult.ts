import { TestStage } from "../TestStage";

export class TestRunResult {
	constructor(
		public title: string,
		public elapsed: number,
		public stage: TestStage,
		public consoleOutput: string,
		public passed: boolean,
		public reason?: string) {
	}
}
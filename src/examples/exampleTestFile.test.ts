import { TestSuite, TestParams } from "just-test-api";

function someFunction() { }

console.log("When discovering, or using json reporter - Should not print in module scope");

export default function (suite: TestSuite): void {
	suite.describe("Passing suite", suite => {
		console.log("When discovering, or using json reporter - Should not print in describe");

		suite.test("Simple test", t => {
			console.log("When discovering, or using json reporter - Should not print in test");
		});

		suite.test("Async", async t => {
			await Promise.resolve("Hi");
		});

		suite.describe(someFunction, suite => {
			suite.test("Test", t => {
			});
		});
	});

	suite.describe("Failing suite", suite => {

		suite.test("No stage error", t => {
			throw "asdf";
		});

		suite.test("Arrage error", t => {
			t.arrange();
			throw "asdf";
		});

		suite.test("Act error", t => {
			t.act();
			throw "asdf";
		});

		suite.test("Assert error", t => {
			t.assert();
			throw "asdf";
		});

		suite.test("Throw Error", t => {
			t.assert();
			throw new Error("Some error message.");
		});

		suite.test("Throws after await", async t => {
			await new Promise(r => setTimeout(r, 1));

			t.assert();
			throw "error";
		});

		suite.test("Timeout reached.", async t => {
			t.timeout = 1;
			await new Promise(r => setTimeout(r, 100));
		});
	});

	suite.describe("Empty suite", suite => {
	});

	suite.describe("Named functions", suite => {
		suite.testFunc(named_function_test_with_spaces);
		suite.testFunc(function inline_function() { });
	});
}

function named_function_test_with_spaces(t: TestParams) {
}

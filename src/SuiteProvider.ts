import * as path from "path";
import { Suite } from "./Suite";

export class SuiteProvider {
	public root: Suite;
	private directorySuites: DirectorySuite[];

	constructor(directorySuiteFilesPaths: string[]) {
		this.root = new Suite();
		this.directorySuites = [];

		const dirs = directorySuiteFilesPaths.map(e => new DirectorySuite(path.resolve(e)));
		dirs.sort(e => e.path.length);

		for (let i = 0; i < dirs.length; i++) {
			const current = dirs[i];
			const parent = this.findParentForDirAt(dirs, i);

			if (parent)
				parent.setAsParentOf(current);
			else
				this.setUnderRoot(current);
		}
	}

	private findParentForDirAt(dirs: DirectorySuite[], i: number): DirectorySuite | null {
		const current = dirs[i];

		for (let j = i - 1; j >= 0; j--) {
			const checkedParent = dirs[j];

			if (current.path.startsWith(checkedParent.path))
				return checkedParent;
		}

		return null;
	}

	private setUnderRoot(dirSuite: DirectorySuite) {
		this.directorySuites.push(dirSuite);
		this.root.addChildSuite(dirSuite.suite);
	}

	public getSuiteForTestFile(filePath: string): Suite {
		filePath = path.resolve(filePath);

		for (const dirSuite of this.directorySuites) {
			const suite = dirSuite.getSuiteOrNull(filePath);

			if (suite)
				return suite;
		}

		return this.root;
	}
}

class DirectorySuite {
	public path: string;
	public suite: Suite;
	private subDirectorySuites: DirectorySuite[];

	constructor(directorySuiteFilePath: string) {
		this.path = path.dirname(directorySuiteFilePath);
		this.subDirectorySuites = [];

		this.suite = new Suite();
		this.suite.title = path.basename(this.path);
		this.suite.location = { filePath: directorySuiteFilePath, start: { line: 0, column: 0 }, end: { line: 0, column: 0 } };
	}

	public setAsParentOf(child: DirectorySuite) {
		this.subDirectorySuites.push(child);
		this.suite.addChildSuite(child.suite);
	}

	public getSuiteOrNull(filePath: string): Suite | null {
		if (!filePath.startsWith(this.path))
			return null;

		for (const subDirSuite of this.subDirectorySuites) {
			const suite = subDirSuite.getSuiteOrNull(filePath);

			if (suite)
				return suite;
		}

		return this.suite;
	}
}
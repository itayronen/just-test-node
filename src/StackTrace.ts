export function setStackRoot(predicate: (callSite: NodeJS.CallSite) => boolean): Function {
	const original = Error.prepareStackTrace;

	Error.prepareStackTrace = (error: Error, callsites: NodeJS.CallSite[]) => {
		const index = callsites.findIndex(predicate)
		callsites = callsites.slice(0, index);

		const callSiteLines = callsites.map(e => formatCallSiteAt(e));
		const callSitesText = callSiteLines.join("\n");

		return `Error: ${error.message}\n${callSitesText}`;
	}

	return () => Error.prepareStackTrace = original;
}

export function getCallerLocationText(currentFunction: Function): string {
	const originalLimit = Error.stackTraceLimit;
	const originalPrepare = Error.prepareStackTrace;

	Error.stackTraceLimit = 1;
	Error.prepareStackTrace = getFirstCallLocation;

	const object = { stack: "", originalPrepare: originalPrepare };
	Error.captureStackTrace(object, currentFunction);
	const location = object.stack;

	Error.stackTraceLimit = originalLimit;
	Error.prepareStackTrace = originalPrepare;

	return location;
}

function getFirstCallLocation(object: any, stack: NodeJS.CallSite[]): string {
	const callSite = stack[0];

	if (!object.originalPrepare)
		return formatCallSite(callSite)

	// Support for ts-node
	const formattedByRuntime: string = object.originalPrepare(object, [new SimplifiedCallSite(callSite)]);

	return formattedByRuntime.slice("Error\n    at ".length);
}

function formatCallSite(callSite: NodeJS.CallSite): string {
	return `${callSite.getFileName()}:${callSite.getLineNumber()}:${callSite.getColumnNumber()}`;
}

function formatCallSiteAt(callSite: NodeJS.CallSite): string {
	const line = (callSite.getLineNumber() ?? 0) + 1;
	const column = (callSite.getColumnNumber() ?? 0) + 1;

	return `\tat ${callSite.getFileName()}:${line}:${column}`;
}

class SimplifiedCallSite {
	constructor(private callSite: NodeJS.CallSite) { }
	getThis(): unknown { return undefined; }
	getTypeName(): string | null { return null; }
	getFunction(): Function | undefined { return undefined; }
	getFunctionName(): string | null { return null }
	getMethodName(): string | null { return null; }
	getFileName(): string | null { return this.callSite.getFileName(); }
	getLineNumber(): number | null { return this.callSite.getLineNumber(); }
	getColumnNumber(): number | null { return this.callSite.getColumnNumber(); }
	getEvalOrigin(): string | undefined { return undefined; }
	isToplevel(): boolean { return true; }
	isEval(): boolean { return false; }
	isNative(): boolean { return false; }
	isConstructor(): boolean { return false; }
}

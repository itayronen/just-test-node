import { OptionDefinition } from "./OptionsDefinitions";

export function printHelp(optionsDefinitions: OptionDefinition[]) {
	console.log();
	console.log("Help");
	console.log();

	optionsDefinitions.forEach(e => {
		let optionsUsage = `--${e.name}`;

		if (e.alias) {
			optionsUsage += `, -${e.alias}`;
		}

		optionsUsage += `\t${e.description}`;

		if (e.defaultValue) {
			optionsUsage += `\t Default: ${e.defaultValue}`;
		}

		console.log(optionsUsage);
		console.log();
	});
}
#!/usr/bin/env node

import { TestSuite } from "just-test-api";
import args from "command-line-args";
import glob from "glob";
import path from "path";
import { Options, optionsDefinitions, validateOptions } from "./OptionsDefinitions";
import { printHelp } from "./help";
import { Suite } from "./Suite";
import { Reporter, SpecReporter, JsonReporter } from "./reporters";
import { SuiteProvider } from "./SuiteProvider";
import { stringifyReason } from "./reason-stringify";

type TestFileFunction = (suite: TestSuite) => void;

main();

async function main() {
	try {
		const options: Options = args(optionsDefinitions) as Options;
		validateOptions(options)

		if (options.help) {
			printHelp(optionsDefinitions);
			return
		}

		const { testFiles, suiteProvider } = await resolveFiles(options);

		if (options.discover) {
			await outputDiscoveredTests(testFiles, suiteProvider);
			return
		}

		const passed = await test(testFiles, suiteProvider, options);

		if (!passed)
			process.exit(options.testFailureExitCode);
	}
	catch (error) {
		console.error(error);
		process.exit(-1);
	}
};

async function resolveFiles(options: Options) {
	const restoreConsole = disableConsole();

	try {
		const testFilesPromise = getTestFilesAsync(options);
		const directorySuitesPromise = getDirectorySuitesAsync(options);

		const directorySuites = await directorySuitesPromise;
		const suiteProvider = new SuiteProvider(directorySuites);
		const testFiles = await testFilesPromise;

		return { testFiles, suiteProvider };
	}
	finally {
		restoreConsole();
	}
}

function disableConsole(): Function {
	const original = console;
	console = createFakeConsole();

	return () => { console = original; };
}

function createFakeConsole(): Console {
	function nothing() { };

	return { log: nothing, info: nothing, debug: nothing, warn: nothing, error: nothing } as Console;
}

async function getTestFilesAsync(options: Options): Promise<string[]> {
	return await globAsync(options.glob);
}

async function getDirectorySuitesAsync(options: Options): Promise<string[]> {
	return await globAsync(options.directorySuitesGlob);
}

function globAsync(pattern: string): Promise<string[]> {
	return new Promise((resolve, reject) => {
		glob(pattern, (error, matches) => {
			if (error)
				reject(error);
			else
				resolve(matches);
		});
	});
}

async function test(files: string[], suiteProvider: SuiteProvider, options: Options): Promise<boolean> {
	const reporter = createReporter(options.reporter, console);

	const restoreConsole = options.reporter == "json" ?
		disableConsole() :
		() => { };
		
	try {
		const suite = await createTree(files, suiteProvider);
		reporter.listenTo(suite);

		if (options.filter)
			suite.treeShake(options.filter.toLowerCase());

		return await suite.run({ debug: options.debug });
	}
	finally {
		restoreConsole();
	}
}

function createReporter(reporter: "spec" | "json", console: Console): Reporter {
	switch (reporter) {
		case "spec":
			return new SpecReporter(console);
		case "json":
			return new JsonReporter(console);
	}
}

async function createTree(filesPaths: string[], suiteProvider: SuiteProvider): Promise<Suite> {
	for (let filePath of filesPaths) {
		const testFuncPromise = getTestFileFunctionAsync(filePath);
		const suite = suiteProvider.getSuiteForTestFile(filePath);

		const testFunc = await testFuncPromise;
		testFunc(suite);
	}

	return suiteProvider.root;
}

async function getTestFileFunctionAsync(file: string): Promise<TestFileFunction> {
	let testFileFunction: any;

	try {
		testFileFunction = await import(path.resolve(file));
	}
	catch (reason) {
		throw `The file "${file}" matched the tests glob but failed to be imported. reason: ${stringifyReason(reason)}`;
	}

	if (typeof testFileFunction != "function")
		testFileFunction = testFileFunction.default;

	if (typeof testFileFunction != "function") {
		throw `The file "${file}" matched the tests glob but did not export a default function.`;
	}

	return testFileFunction;
}

async function outputDiscoveredTests(files: string[], suiteProvider: SuiteProvider): Promise<void> {
	const restoreConsole = disableConsole();
	let suite: Suite;

	try {
		suite = await createTree(files, suiteProvider);
	}
	finally {
		restoreConsole();
	}

	console.log(JSON.stringify(suite.discover()));
}

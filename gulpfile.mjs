"use strict";
import path from "path";
import gulp from "gulp";
import ts from 'gulp-typescript';
import sourcemaps from 'gulp-sourcemaps';
import read from "gulp-read";
import del from "del";
import changed from "itay-gulp-changed";
import merge from "merge2";

let config = {
	src: "./src",
	tsconfig: "./src/tsconfig.json",
	tsGlob: "./src/**/*.ts",
	testSuitesGlob: "./src/**/@test-suite",
	dest: "./lib",
};

gulp.task("build", function () {
	const tsProject = ts.createProject(config.tsconfig, { declaration: true });

	const tsResult = gulp.src(config.tsGlob, { read: false })
		.pipe(changed())
		.pipe(read())
		.pipe(sourcemaps.init())
		.pipe(tsProject())
		;

	tsResult.on("error", () => {
		changed.reset();
		throw "Typescript build failed.";
	});

	tsResult.js = tsResult.js.pipe(sourcemaps.write({
		includeContent: false,
		sourceRoot: file => jsFileToSrcFolder(file)
	}));

	const copyTestSuiteFiles = gulp.src(config.testSuitesGlob, { read: false })
		.pipe(changed({ key: "test-suites" }))
		.pipe(read())
		;

	return merge([
		tsResult.dts.pipe(gulp.dest(config.dest)),
		tsResult.js.pipe(gulp.dest(config.dest)),
		copyTestSuiteFiles.pipe(gulp.dest(config.dest))
	]);
});

function jsFileToSrcFolder(file) {
	const jsPath = path.join(config.dest, path.dirname(file.relative));

	return path.relative(jsPath, config.src);
}

gulp.task("clean", function () {
	return del([
		config.dest + '**/*',
		"./.localStorage"
	]);
});